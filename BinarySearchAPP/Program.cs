﻿using System;
using System.Linq;

namespace BinarySearchAPP
{
    internal static class Program
    {
        private static void Main()
        {
            var binarySearch = new BinarySearch();
            

            var arr = Enumerable.Range(0, 100000000).ToArray();
            var targetNumber = 1908745;

            var recursiveBinarySearchTime = new System.Diagnostics.Stopwatch();
            var recursiveBinarySearchResult = binarySearch.BinarySearchRecursive(arr, 0, arr.Length - 1, targetNumber);
            recursiveBinarySearchTime.Stop();

            var nonRecursiveBinarySearchTime = new System.Diagnostics.Stopwatch();
            var nonRecursiveBinarySearchResult = binarySearch.BinarySearchNonRecursive(arr, targetNumber);
            nonRecursiveBinarySearchTime.Stop();

            var dotNetBinarySearchTime = new System.Diagnostics.Stopwatch();
            var dotNetBinarySearchResult = Array.BinarySearch(arr, targetNumber);
            dotNetBinarySearchTime.Stop();

            Console.WriteLine($"Execution Time: {recursiveBinarySearchTime.ElapsedMilliseconds} ms");
            Console.WriteLine($"Element found at index: {recursiveBinarySearchResult}");
            Console.WriteLine($"Execution Time: {nonRecursiveBinarySearchTime.ElapsedMilliseconds} ms");
            Console.WriteLine($"Element found at index: {nonRecursiveBinarySearchResult}");
            Console.WriteLine($"Execution Time: {dotNetBinarySearchTime.ElapsedMilliseconds} ms");
            Console.WriteLine($"Element found at index: {dotNetBinarySearchResult}");

            Console.ReadLine();
        }
    }
}
