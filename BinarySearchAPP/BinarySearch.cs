﻿using System;
using System.Collections.Generic;

namespace BinarySearchAPP
{
    public class BinarySearch
    {
       public int BinarySearchRecursive(IReadOnlyList<int> array, int indexMin, int indexMax, int targetNumber)
        {

            if (indexMax >= indexMin)
            {
                var indexMiddle = (indexMin + indexMax) / 2;

                if (array[indexMiddle] == targetNumber)
                {
                    return indexMiddle;
                }

                return array[indexMiddle] > targetNumber
                           ? BinarySearchRecursive(array,
                                          indexMin,
                                          indexMiddle - 1,
                                          targetNumber)
                           : BinarySearchRecursive(array,
                                          indexMiddle + 1,
                                          indexMax,
                                          targetNumber);
            }

            throw new ArgumentOutOfRangeException($"Target number  {targetNumber} is not parameter of the array.");

        }

       public int BinarySearchNonRecursive(IReadOnlyList<int> array, int targetNumber)
        {
            int indexMin = 0, indexMax = array.Count - 1;
            while (indexMin <= indexMax)
            {

                var indexMiddle = (indexMin + indexMax) / 2;
                if (array[indexMiddle] == targetNumber)
                {
                    return indexMiddle;
                }

                if (array[indexMiddle] > targetNumber)
                {
                    indexMax = indexMiddle - 1;
                }
                else
                {
                    indexMin = indexMiddle + 1;
                }
            }

            throw new ArgumentOutOfRangeException($"Target number  {targetNumber} is not parameter of the array.");
        }
    }
}