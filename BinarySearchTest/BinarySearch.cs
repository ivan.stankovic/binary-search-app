using System;
using BinarySearchAPP;
using NUnit.Framework;

namespace BinarySearchTest
{
    public class Tests
    {
        private BinarySearch _binarySearch;
        private int[] _array;

        [SetUp]
        public void Setup()
        {
             _binarySearch = new BinarySearch();
            _array = new[]
                     {
                          2,
                          3,
                          5,
                          7,
                          11,
                          13,
                          17,
                          19,
                          23,
                          29,
                          31,
                          37,
                          41,
                          43,
                          47,
                          53,
                          59,
                          61,
                          67,
                          71,
                          73,
                          79,
                          83,
                          89,
                          97
                      };
        }

        [Test]
        public void BinarySearchRecursive_When_TargetValueExist()
        {
            //Act
            var result = _binarySearch.BinarySearchRecursive(_array,
                                                             0,
                                                             _array.Length - 1,
                                                             73);

            //Assert
            Assert.AreEqual(20, result);
        }


        [Test]
        public void BinarySearchNonRecursive_When_TargetValueExist()
        {
            //Act
            var result = _binarySearch.BinarySearchNonRecursive(_array,
                                                             73);

            //Assert
            Assert.AreEqual(20, result);
        }

        [Test]
        public void BinarySearchRecursive_When_TargetValueIsOutOfRange_ExceptionIsThrown()
        {

            //Assert
            Assert.Throws<ArgumentOutOfRangeException>(() => _binarySearch.BinarySearchRecursive(_array,
                                                                                                 0,
                                                                                                 _array.Length - 1,
                                                                                                 int.MaxValue));
        }

        [Test]
        public void BinarySearchNonRecursive_When_TargetValueIsOutOfRange_ExceptionIsThrown()
        {

            //Assert
            Assert.Throws<ArgumentOutOfRangeException>(() => _binarySearch.BinarySearchNonRecursive(_array,
                                                                                                 int.MinValue));
        }
    }
}